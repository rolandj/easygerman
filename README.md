# Fullstack language learning app
## [Live version](https://easygerman.herokuapp.com)
## Concept:
Two types of users: _teacher_ and _student_
### Lesson
Each lesson contains words in target language and their translations.
### Teacher
* manages lessons,
* creates new content.
### Student
* learns new words everyday and selects grades,
* keeps repeating given word until has majority of good grades,
* when every word within lesson is learned, student could overview or reset lesson.

#### API documentation (in progress)
https://easygerman.herokuapp.com/api/docs/