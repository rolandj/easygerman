from djongo import models
from django import forms

from cloudinary.models import CloudinaryField


class Word(models.Model):
    word = models.CharField(max_length=50)
    translation = models.CharField(max_length=50)

    class Meta:
        abstract = True

    def __str__(self):
        return self.word

    def __eq__(self, other):
        return self.word == other.word


class WordForm(forms.ModelForm):
    class Meta:
        model = Word
        fields = ('word', 'translation')


class Lesson(models.Model):
    """Represent lesson.

    Stores name, enrolled students and lesson's core - array of words and their translations in flat representation.
    """
    name = models.CharField(unique=True, max_length=100)
    students = models.ForeignKey('authentication.User', on_delete=models.PROTECT)
    words = models.ArrayField(model_container=Word, model_form_class=WordForm)
    image = CloudinaryField('image', null=True, blank=True)
    bg_width = models.PositiveIntegerField(null=True, blank=True)
    bg_height = models.PositiveIntegerField(null=True, blank=True)
    objects = models.DjongoManager()  # Custom object field, obligatory when using Djongo

    def __str__(self):
        return f'{self.name}'
