from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, JSONParser

from easygerman.apps.grades.serializers import ProgressSerializer
from .serializers import LessonSerializer, LessonCreateUpdateSerializer, LessonListSerializer
from .services import get_words_to_learn, reset_lesson
from .models import Lesson
from .permissions import IsTeacherOrReadOnly, IsProgressOwner, IsStudent


class LessonViewSet(viewsets.ModelViewSet):
    permission_classes = (IsTeacherOrReadOnly,)
    queryset = Lesson.objects.all()
    parser_classes = (MultiPartParser, JSONParser)

    def get_serializer_class(self):
        if self.action == 'list':
            return LessonListSerializer
        elif self.action in ('create', 'update', 'partial_update'):
            return LessonCreateUpdateSerializer
        else:
            return LessonSerializer

    @action(detail=True, methods=['get'], url_path='start', permission_classes=(IsStudent,))
    def get_words_to_learn(self, request, pk=None):
        """Endpoint for getting words within given lesson that student needs to learn.

        Implicitly creates new `Progress` object when user selects specified lesson for first time."""
        data, lesson_id = get_words_to_learn(request, pk)
        serializer = ProgressSerializer({'grades': data, 'id': lesson_id})
        return Response(serializer.data)

    @action(detail=True, methods=['post'], url_path='reset', permission_classes=(IsProgressOwner,))
    def reset(self, request, pk=None):
        """Endpoint for resetting student's progress in specified lesson.

        Deletes student's `Progress` instance bound to given lesson."""
        reset_lesson(request, pk)
        return Response(status=status.HTTP_204_NO_CONTENT)
