from rest_framework.permissions import BasePermission

SAFE_METHODS = ['GET', 'HEAD', 'OPTIONS']


class IsTeacherOrReadOnly(BasePermission):
    """The request is authenticated as a teacher, or is a read-only request"""

    def has_permission(self, request, view):
        if request.user.is_authenticated:
            return request.user.is_teacher or request.method in SAFE_METHODS

        return request.method in SAFE_METHODS


class IsProgressOwner(BasePermission):
    """Return true when request user is instance owner and method is POST"""

    def has_permission(self, request, view):
        return request.method == "POST" and request.user.is_authenticated


class IsStudent(BasePermission):
    """Return true when request method is GET and user is authenticated student"""

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS and request.user.is_authenticated:
            return request.user.is_student
        return False
