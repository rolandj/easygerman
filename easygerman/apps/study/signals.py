from django.dispatch import Signal

# Signal sent when `Lesson` instance was updated
# Tells `Progress` objects to update their keys
LESSON_UPDATED = Signal()
