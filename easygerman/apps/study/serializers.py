from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Lesson, Word
from .signals import LESSON_UPDATED


class WordSerializer(serializers.Serializer):
    word = serializers.CharField(max_length=20)
    translation = serializers.CharField(max_length=20)


class LessonSerializer(serializers.ModelSerializer):
    words = serializers.ListField(child=WordSerializer())
    background = serializers.SerializerMethodField()

    class Meta:
        model = Lesson
        fields = ('id', 'url', 'name', 'words', 'background')

    def get_background(self, instance):
        if hasattr(instance.image, 'url'):
            return instance.image.url


class LessonListSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()

    class Meta:
        model = Lesson
        fields = ('id', 'name', 'url')

    def get_url(self, obj):
        return reverse('lesson-detail', args=[obj.id], request=self.context['request'])


class LessonCreateUpdateSerializer(serializers.ModelSerializer):
    words = serializers.ListField(child=WordSerializer())
    background = serializers.ImageField(allow_null=True, required=False)

    class Meta:
        model = Lesson
        fields = ('id', 'url', 'name', 'words', 'background')

    def create(self, validated_data):
        name = validated_data.get('name')
        words = [Word(**w) for w in validated_data.get('words')]
        background = validated_data.get('background')
        return Lesson.objects.create(name=name, words=words, image=background)

    def update(self, instance, validated_data):
        words = validated_data.get('words')
        background = validated_data.get('background')
        if words:
            instance.words = [Word(**w) for w in validated_data.get('words')]
            instance.save()
            LESSON_UPDATED.send(sender=self.__class__, lesson=instance, words=words)
        if background:
            instance.image = background
            instance.save()
        return instance

    def get_background(self, instance):
        if hasattr(instance.image, 'url'):
            return instance.image.url
