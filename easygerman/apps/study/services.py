from easygerman.apps.grades.models import Progress, Grade
from .models import Lesson


def get_words_to_learn(request, lesson_id) -> tuple:
    """Find words that user needs to practice"""
    lesson = Lesson.objects.get(pk=lesson_id)
    try:
        instance = Progress.objects.get(student=request.user, lesson=lesson)
    except Progress.DoesNotExist:
        grades = [Grade(word=w.word, translation=w.translation) for w in lesson.words]
        instance = Progress.objects.create(lesson=lesson, student=request.user, grades=grades)
        return grades, instance.id
    else:
        words_to_learn = [grade for grade in instance.grades if not grade.learned]
        return words_to_learn, instance.id


def reset_lesson(request, lesson_id) -> tuple:
    """Reset student's `Progress` record related to specified lesson"""
    instance = Progress.objects.get(student=request.user, lesson=lesson_id)
    return instance.delete()
