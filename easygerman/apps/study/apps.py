from django.apps import AppConfig


class StudyConfig(AppConfig):
    name = 'easygerman.apps.study'
