from rest_framework.routers import DefaultRouter
from .api import LessonViewSet


router = DefaultRouter()
router.register('', LessonViewSet)
urlpatterns = router.urls
