from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, DestroyModelMixin, ListModelMixin
from rest_framework.viewsets import GenericViewSet
from rest_framework.views import Response
from rest_framework.permissions import IsAuthenticated

from .serializers import ProgressSerializer, ProgressCreateSerializer, ProgressListSerializer
from .models import Progress
from .services import patch_grade


class ProgressViewSet(RetrieveModelMixin, DestroyModelMixin, ListModelMixin, GenericViewSet):
    """Endpoint for processing student's grades."""
    default_serializer_class = ProgressSerializer
    permission_classes = (IsAuthenticated,)
    serializer_classes = {
        'list': ProgressListSerializer,
        'create': ProgressCreateSerializer,
    }

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)

    def get_queryset(self):
        return Progress.objects.filter(student=self.request.user)

    def partial_update(self, request, *args, **kwargs):
        """Overridden default patch as processed data must meet specific standards

        Each `Grade` object contains good / medium / wrong counters which could be only incremented."""
        instance = self.get_object()
        serializer = self.get_serializer(data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        # Instead of 'perform_update', handle patching grades manually
        instance = patch_grade(instance, serializer.validated_data)
        output_serializer = self.get_serializer(instance)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(output_serializer.data)
