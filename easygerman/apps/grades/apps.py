from django.apps import AppConfig


class GradesConfig(AppConfig):
    name = 'easygerman.apps.grades'
