from djongo import models
from django import forms

from easygerman.apps.study.models import Lesson


class Grade(models.Model):
    word = models.CharField(max_length=50)
    translation = models.CharField(max_length=50)
    good = models.IntegerField(default=0)
    medium = models.IntegerField(default=0)
    wrong = models.IntegerField(default=0)

    class Meta:
        abstract = True

    @property
    def learned(self):
        return self.good > self.wrong + 0.6 * self.medium

    def __str__(self):
        return self.word

    def __eq__(self, other):
        return self.word == other.word


class GradeForm(forms.ModelForm):
    class Meta:
        model = Grade
        fields = ('word', 'translation', 'good', 'medium', 'wrong')


class Progress(models.Model):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    student = models.ForeignKey('authentication.User', on_delete=models.CASCADE)
    grades = models.ArrayField(model_container=Grade, model_form_class=GradeForm)

    objects = models.DjongoManager()

    @property
    def percentage(self):
        return (len([g for g in self.grades if g.learned]) / len(self.grades)) * 100

    def __str__(self):
        return f"{self.student}'s progress in {self.lesson}"
