from rest_framework.routers import DefaultRouter
from .api import ProgressViewSet

router = DefaultRouter()
router.register('', ProgressViewSet, basename='progress')
urlpatterns = router.urls
