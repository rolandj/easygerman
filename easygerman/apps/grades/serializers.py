from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import Progress, Grade


class GradeSerializer(serializers.Serializer):
    word = serializers.CharField(max_length=50)
    translation = serializers.CharField(max_length=50)
    good = serializers.IntegerField()
    medium = serializers.IntegerField()
    wrong = serializers.IntegerField()


class ProgressSerializer(serializers.HyperlinkedModelSerializer):
    grades = serializers.ListField(child=GradeSerializer())

    class Meta:
        model = Progress
        fields = ('id', 'percentage', 'lesson', 'student', 'grades')
        read_only_fields = ('id', 'lesson', 'percentage', 'student')
        extra_kwargs = {'student': {'lookup_field': 'username'}}


class ProgressCreateSerializer(serializers.ModelSerializer):
    grades = serializers.ListField(child=GradeSerializer(), required=False)

    class Meta:
        model = Progress
        fields = ('id', 'percentage', 'lesson', 'student', 'grades')
        read_only_fields = ('id', 'percentage', 'student')

    def create(self, validated_data):
        lesson = validated_data.get('lesson')
        student = self.context['request'].user
        grades = [Grade(w.word) for w in lesson.words]
        return Progress.objects.create(lesson=lesson, student=student, grades=grades)


class ProgressListSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    lesson_name = serializers.CharField(source="lesson.name")

    class Meta:
        model = Progress
        fields = ('id', 'lesson', 'lesson_name', 'percentage', 'url')
        read_only_fields = ('id', 'lesson', 'percentage', 'url', 'lesson_name')

    def get_url(self, obj):
        return reverse('progress-detail', kwargs={'username': obj.student.username, 'pk': obj.id})
