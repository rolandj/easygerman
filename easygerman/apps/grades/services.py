from django.dispatch import receiver
from rest_framework.exceptions import NotFound, NotAcceptable

from .models import Grade, Progress
from ..study.signals import LESSON_UPDATED
from ..study.models import Lesson, Word


def patch_grade(instance: Progress, data):
    """Perform patch on `Progress` instance.

    All data must be saved in this function since it is not out of the box implemented in DRF to update
    nested objects.
    :param Progress instance: instance to patch
    :param data: serializer's validated data
    :raises NotAcceptable: when client does not increment any counter
    """
    input_grades = [Grade(**entry) for entry in data.get('grades')]

    for old in instance.grades:
        for grade in input_grades:
            if grade not in instance.grades:
                raise NotFound(detail='Grade for word {} not found.'.format(grade.word))

            if grade == old:
                if old.good != grade.good and old.good + 1 == grade.good:
                    old.good = grade.good
                    break

                if old.medium != grade.medium and old.medium + 1 == grade.medium:
                    old.medium = grade.medium
                    break

                if old.wrong != grade.wrong and old.wrong + 1 == grade.wrong:
                    old.wrong = grade.wrong
                    break

                if old.good != grade.good or old.medium != grade.medium or old.wrong != grade.wrong:
                    raise NotAcceptable(detail='Counter could be only incremented.')

    instance.save()


@receiver(LESSON_UPDATED)
def update_grades_keys(words: list, lesson: Lesson, **kwargs):
    """Update all `Progress` instances when lesson is changed.

    Callback function with large complexity thus lessons should be updated rarely.
    :param list words: words that have been changed
    :param Lesson lesson: lesson that has been updated
    """
    for instance in Progress.objects.filter(lesson=lesson):

        for grade in instance.grades.copy():
            if grade not in words:
                instance.grades.remove(grade)

        for w in words:
            if instance not in instance.grades:
                instance.grades.append(Grade(word=w.word, translation=w.translation))

        instance.save()
