from rest_framework import serializers
from .tasks import send_confirm_email

from .models import User


class UserSerializer(serializers.ModelSerializer):
    progress = serializers.HyperlinkedIdentityField(view_name='progress-list', lookup_field='username')

    class Meta:
        model = User
        fields = ('id', 'username', 'is_teacher', 'email', 'progress')
        read_only_fields = ('id', 'is_student',)


class UserCreateSerializer(serializers.ModelSerializer):
    activated = serializers.BooleanField(source='is_active', required=False)

    class Meta:
        model = User
        read_only_fields = ('is_teacher', 'activated')
        fields = ('id', 'password', 'username', 'is_teacher', 'email', 'activated')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """ Create and return new user.

         Sends confirmation email. User instance is not active by default."""
        user = User(
            username=validated_data.get('username'),
            email=validated_data.get('email'),
            is_active=False
        )
        user.set_password(validated_data.get('password'))
        user.save()
        send_confirm_email(user)
        return user
