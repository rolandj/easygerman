from django.core.mail import send_mail
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from .tokens import generator


def send_confirm_email(user):
    """Sends confirmation email to user.

    Generates url with token and encoded user ID in query params.
    """
    token = generator.make_token(user)
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    url = f'https://easygerman.herokuapp.com/activate/?token={token}&uid={uid}'
    message = f'Hello {user.username}, \n' \
              f"\n we're glad you want to learn german with us! " \
              f'Please click the following link to activate your account:\n{url}'
    send_mail(
        subject='EasyGerman account activation',
        message=message,
        from_email='EasyGerman <noreply@easygerman.herokuapp.com>',
        recipient_list=[user.email]
    )
