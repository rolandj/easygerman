from django.apps import AppConfig


class AuthenticationConfig(AppConfig):
    name = 'easygerman.apps.authentication'
