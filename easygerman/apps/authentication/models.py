from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """Represent user as teacher or student with email as possible credential to login"""
    is_student = models.BooleanField(default=True)
    is_teacher = models.BooleanField(default=False)
    email = models.EmailField(unique=True)

    def __str__(self):
        return self.username
