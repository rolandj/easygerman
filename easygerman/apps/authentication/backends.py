from django.contrib.auth.backends import ModelBackend

from .models import User


class EmailAuthBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        """Authenticate user with email or username"""
        if username is None or password is None:
            return None

        try:
            user = User.objects.get(email=username)
            if user.check_password(password) and user.is_active:
                return user
        except User.DoesNotExist:
            try:
                user = User.objects.get(username=username)
                if user.check_password(password) and user.is_active:
                    return user
            except User.DoesNotExist:
                return None
