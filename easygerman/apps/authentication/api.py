from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.shortcuts import redirect

from rest_framework import viewsets, permissions
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_401_UNAUTHORIZED
from rest_framework.views import APIView
from rest_framework.exceptions import NotFound, PermissionDenied

from .models import User
from .serializers import UserSerializer, UserCreateSerializer
from .tokens import generator


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    lookup_field = 'username'

    def get_serializer_class(self):
        return UserCreateSerializer if self.request.method == 'POST' else UserSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return User.objects.all() if self.request.user.is_superuser else [self.request.user]
        raise PermissionDenied

    def get_object(self):
        if self.kwargs.get('username') == self.request.user.username:
            return self.request.user
        raise NotFound


class Login(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'username': user.username,
            'is_teacher': user.is_teacher
        })


class Activate(APIView):
    """A view for handling account activation."""

    def get(self, request, *args, **kwargs):
        """Validates token and activates user account."""
        token = request.query_params['token']
        uidb64 = request.query_params['uid']

        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except User.DoesNotExist:
            return Response(status=HTTP_404_NOT_FOUND)

        if generator.check_token(user, token):
            user.is_active = True
            user.save()
            return redirect('success/')
        else:
            return Response(status=HTTP_401_UNAUTHORIZED)
