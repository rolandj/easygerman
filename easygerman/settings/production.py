from .base import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.environ['SECRET_KEY']

DEBUG = False

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ALLOWED_HOSTS = [os.environ['HOST']]

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'rest_framework.authtoken',
    'djongo',
    'factory',
    'drf_yasg',
    'anymail',

    'easygerman.apps.authentication.apps.AuthenticationConfig',
    'easygerman.apps.study.apps.StudyConfig',
    'easygerman.apps.grades.apps.GradesConfig',
]

# Database

DATABASES = {
    'default': {
        'ENGINE': 'djongo',
        'NAME': os.environ['DB_NAME'],
        'ENFORCE_SCHEMA': False,
        'CLIENT': {
            'host': os.environ['MONGODB_URI'],
            'retryWrites': 'false',
        }
    }
}

# REST Framework

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticatedOrReadOnly'
    ],
    'DEFAULT_THROTTLE_CLASSES': [
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle'
    ],
    'DEFAULT_THROTTLE_RATES': {
        'anon': '10/minute',
        'user': '100/minute'
    }
}

# Anymail settings

ANYMAIL = {
    "MAILGUN_API_KEY": os.environ['MAILGUN_API_KEY'],
    "MAILGUN_SENDER_DOMAIN": os.environ['MAILGUN_DOMAIN'],
}

EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"
DEFAULT_FROM_EMAIL = "no_reply@easygerman.herokuapp.com"
