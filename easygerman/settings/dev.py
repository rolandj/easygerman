from .base import *

DEBUG = True

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ALLOWED_HOSTS = []

INSTALLED_APPS.append('anymail')

# Database

DATABASES = {
    'default': {
        'ENGINE': 'djongo',
        'NAME': 'easygerman',
        'ENFORCE_SCHEMA': False,
        'CLIENT': {
            'host': 'mongodb://dev:password@127.0.0.1:27017/',
        }
    }
}

# REST Framework

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ],
}

# Cors on local development

CORS_ORIGIN_ALLOW_ALL = True
