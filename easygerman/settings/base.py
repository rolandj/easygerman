"""easygerman abstract settings"""
import os
from pathlib import Path

BASE_DIR = Path(os.path.dirname(__file__)).parents[1]

SECRET_KEY = os.environ['SECRET_KEY']

DEBUG = ""

# Application definition

MIDDLEWARE = []

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'rest_framework.authtoken',
    'djongo',
    'factory',
    'corsheaders',
    'drf_yasg',
    'cloudinary',

    'easygerman.apps.authentication.apps.AuthenticationConfig',
    'easygerman.apps.study.apps.StudyConfig',
    'easygerman.apps.grades.apps.GradesConfig',
]

ROOT_URLCONF = 'easygerman.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'build')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'easygerman.wsgi.application'

# Database

DATABASES = {}

# Password validation

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Custom authentication

AUTH_USER_MODEL = 'authentication.User'
AUTHENTICATION_BACKENDS = ('easygerman.apps.authentication.backends.EmailAuthBackend',)

# REST Framework

REST_FRAMEWORK = {}

# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Berlin'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'build', 'static'),
]

# Cors on local development

CORS_ORIGIN_ALLOW_ALL = True
