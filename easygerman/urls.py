from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import re_path, path, include

from rest_framework import permissions
from rest_framework.authtoken.views import ObtainAuthToken
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from easygerman.apps.authentication.api import Login, Activate
from easygerman.apps.authentication.urls import urlpatterns as authentication
from easygerman.apps.study.urls import urlpatterns as study_module
from easygerman.apps.grades.urls import urlpatterns as grades
from easygerman.apps.frontend.views import index

schema_view = get_schema_view(
    openapi.Info(
        title="EasyGerman API",
        default_version='v1',
        description="Please note this document is currently developed and does not fully describe API mechanics.",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('activate/', Activate.as_view(), name='user-activation'),
    path('api/admin/', admin.site.urls),

    path('api/lessons/', include(study_module)),
    path('api/users/login/', Login.as_view()),
    path('api/users/', include(authentication)),
    path('api/users/<str:username>/progress/', include(grades)),

    path('api/obtain-token/', ObtainAuthToken.as_view()),
    path('api/docs/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path('.*', index),
]
