import factory

from easygerman.apps.authentication.models import User
from easygerman.apps.grades.models import Progress, Grade
from easygerman.apps.study.models import Lesson, Word


class StudentUserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('user_name')
    email = factory.Faker('email')
    password = factory.Faker('password')
    is_student = True
    is_active = False


class TeacherUserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Faker('user_name')
    email = factory.Faker('email')
    password = factory.Faker('password')
    is_teacher = True


class GradeFactory(factory.Factory):
    class Meta:
        model = Grade

    word = factory.Faker('word')
    translation = factory.Faker('word')
    good = factory.Faker('pyint')
    medium = factory.Faker('pyint')
    wrong = factory.Faker('pyint')


class WordFactory(factory.Factory):
    class Meta:
        model = Word

    word = factory.Faker('word')
    translation = factory.Faker('word')


class LessonFactory(factory.DjangoModelFactory):
    class Meta:
        model = Lesson

    name = factory.Faker('word')
    students = factory.SubFactory(StudentUserFactory)
    words = factory.List([WordFactory() for i in range(20)])


class ProgressFactory(factory.DjangoModelFactory):
    class Meta:
        model = Progress

    student = factory.SubFactory(StudentUserFactory)
    lesson = factory.SubFactory(LessonFactory)
    grades = factory.List([GradeFactory() for i in range(10)])
