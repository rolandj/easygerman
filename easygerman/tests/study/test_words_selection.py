from unittest import mock
from django.test import TestCase

from rest_framework.test import APIRequestFactory

from easygerman.apps.study.services import get_words_to_learn
from easygerman.tests.factories import GradeFactory, LessonFactory, ProgressFactory, StudentUserFactory


class TestWordsToLearnSelection(TestCase):
    """Test selecting words that student needs to practice.

    As some `Grade` instances could be marked as learned, test if student
    gets only words that he has not already learned.
    """

    def setUp(self) -> None:
        self.request = APIRequestFactory()
        self.students = [StudentUserFactory() for i in range(10)]
        self.new_student = StudentUserFactory()
        self.lessons = [LessonFactory() for i in range(5)]
        self.progresses = []

        for lesson in self.lessons:
            for student in self.students:
                grades = [GradeFactory(word=w.word) for w in lesson.words]
                # Create `Progress` instances to test grade selecting multiple times
                self.progresses.append(ProgressFactory(lesson=lesson, grades=grades, student=student))

    def test_all_words_selected_when_no_progress_instance(self):
        self.request.user = self.new_student
        for lesson in self.lessons:
            selected, _ = get_words_to_learn(self.request, lesson.id)
            self.assertEqual(len(selected), len(lesson.words))

    def test_progress_instance_created_when_learning_new_lesson(self):
        self.request.user = self.new_student
        with mock.patch('easygerman.apps.study.services.Progress.objects.create') as progress_created:
            for lesson in self.lessons:
                get_words_to_learn(self.request, lesson.id)
                progress_created.assert_called()

    def test_learned_words_are_not_selected(self):
        for progress in self.progresses:

            learned_counter = 0
            for grade in progress.grades:
                if grade.learned:
                    learned_counter += 1
            progress.save()

            self.request.user = progress.student
            selected, _ = get_words_to_learn(self.request, progress.lesson.id)

            self.assertEqual(len(selected), len(progress.lesson.words) - learned_counter)
