from unittest import mock

from django.test import TestCase
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from rest_framework.test import APIClient

from easygerman.apps.authentication.tokens import generator
from easygerman.tests.factories import StudentUserFactory


class AuthenticationApiTest(TestCase):
    def setUp(self) -> None:
        self.form_data = {
            'username': 'easygerman_user',
            'email': 'test@email.com',
            'password': 'test_password',
        }
        self.url = reverse('user-list')
        self.api_client = APIClient()
        self.user = StudentUserFactory()
        self.token = generator.make_token(self.user)
        self.uid = urlsafe_base64_encode(force_bytes(self.user.pk))

    @mock.patch('easygerman.apps.authentication.tasks.send_mail')
    def test_email_is_sent_to_user_after_signup(self, send_mail_mock):
        self.api_client.post(self.url, data=self.form_data)
        send_mail_mock.assert_called_once()

    @mock.patch('easygerman.apps.authentication.tasks.send_mail')
    def test_user_is_not_active_after_signup(self, send_mail_mock):
        response = self.api_client.post(self.url, data=self.form_data)
        self.assertEqual(response.data['activated'], False)

    def test_user_activates_account_with_sent_url(self):
        url = f'{reverse("user-activation")}?token={self.token}&uid={self.uid}'
        response = self.api_client.get(url)
        self.assertRedirects(response, f'{reverse("user-activation")}success/')

    def test_sent_token_can_be_used_only_once(self):
        url = f'{reverse("user-activation")}?token={self.token}&uid={self.uid}'
        self.api_client.get(url)  # First call
        response = self.api_client.get(url)  # Second activation attempt, token invalid
        self.assertEqual(response.status_code, 401)
