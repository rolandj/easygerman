from django.test import TestCase

from easygerman.apps.grades.services import update_grades_keys
from easygerman.tests.factories import ProgressFactory, LessonFactory, WordFactory, GradeFactory


class TestGradeKeysUpdate(TestCase):
    """Examine if `Grade` objects correctly update their keys.

    As `Grade` instance is mapped to specified word, it needs to update
    this mapping after `Word` instance is changed. This test emphasizes
    testing `update_grade_keys` function which is fired by signal sent
    in `LessonSerializer`.
    """

    def setUp(self) -> None:
        self.lesson = LessonFactory()
        grades = [GradeFactory(word=w.word) for w in self.lesson.words]
        self.progresses = [ProgressFactory(lesson=self.lesson, grades=grades) for i in range(30)]

    def test_grades_not_updated_when_words_dont_change(self):
        # First scenario: none of words are updated
        # Possible when teacher clicks 'send' button, without changing actual Lesson
        updated_keys = self.lesson.words
        update_grades_keys(words=updated_keys, lesson=self.lesson)

        for instance in self.progresses:
            self.assert_no_trash_entries(instance.grades, self.lesson.words)
            for grade, word in zip(instance.grades, self.lesson.words):
                self.assertEqual(grade, word)

    def test_grades_updated_after_words_updated(self):
        # Second scenario: some words are updated
        updated_keys = [WordFactory() for i in range(4)] + self.lesson.words[4:]
        update_grades_keys(lesson=self.lesson, words=updated_keys)

        for instance in self.progresses:
            self.assert_no_trash_entries(instance.grades, self.lesson.words)
            for grade, word in zip(instance.grades, self.lesson.words):
                self.assertEqual(grade, word)

    def test_grades_updated_after_all_words_updated(self):
        # Third scenario: all words are updated
        updated_keys = [WordFactory() for i in self.lesson.words]
        update_grades_keys(words=updated_keys, lesson=self.lesson)

        for instance in self.progresses:
            self.assert_no_trash_entries(instance.grades, self.lesson.words)
            for grade, word in zip(instance.grades, self.lesson.words):
                self.assertEqual(grade, word)

    def assert_no_trash_entries(self, instance_grades, lesson_words):
        self.assertEqual(len(instance_grades), len(lesson_words))
