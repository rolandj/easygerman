from django.test import TestCase
from django.test.client import RequestFactory

from rest_framework.exceptions import NotAcceptable, NotFound

from easygerman.apps.grades.services import patch_grade
from easygerman.apps.grades.serializers import ProgressSerializer
from easygerman.tests.factories import ProgressFactory, GradeFactory


class TestGradeUpdate(TestCase):
    """Test `Grade` instance patching.

    As `Grade` instance holds counters reflecting student's choices,
    those counters could be only incremented.
    """

    def setUp(self) -> None:
        context = {'request': RequestFactory().get('/')}
        self.instance = ProgressFactory()
        self.instance_serialized = ProgressSerializer(self.instance, context=context)
        self.patch_data = self.instance_serialized.data

    def test_grade_could_be_incremented(self):
        old_value = self.instance.grades[0].good

        self.patch_data['grades'][0]['good'] += 1
        patch_grade(self.instance, self.patch_data)

        self.assertEqual(self.instance.grades[0].good, old_value + 1)

    def test_grade_could_be_only_incremented(self):
        # Could not add to much to grade counter
        with self.assertRaises(NotAcceptable):
            self.patch_data['grades'][0]['good'] = 3
            patch_grade(self.instance, self.patch_data)

        # Could not make grade counter lower
        with self.assertRaises(NotAcceptable):
            self.patch_data['grades'][0]['good'] = 0
            patch_grade(self.instance, self.patch_data)

    def test_not_existing_grade_raises(self):
        with self.assertRaises(NotFound):
            self.patch_data['grades'][0]['word'] = 'NotExisting'
            patch_grade(self.instance, self.patch_data)

    def test_grade_marked_as_learned(self):
        grades = [GradeFactory() for i in range(100)]
        for grade in grades:
            self.assertEqual(grade.learned, grade.good > grade.wrong + 0.6 * grade.medium)
