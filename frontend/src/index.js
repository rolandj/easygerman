import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";
import {configureStore} from "@reduxjs/toolkit";
import {Provider} from "react-redux";
import './styles/index.scss';
import authSlice from './components/authentication/authSlice';
import popupSlice from './components/popups/popupSlice';
import lessonSlice from './components/learning/lessonSlice'
import App from './components/App';


export const store = configureStore({
  reducer: {
    auth: authSlice,
    popups: popupSlice,
    lessons: lessonSlice,
  }
});

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
