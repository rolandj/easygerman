import React from "react";
import {Link, Redirect} from "react-router-dom";
import {connect} from "react-redux";

import AuthAction from "./authentication/AuthAction";

import Gate from "../img/brandenburg-gate.png";
import Language from "../img/language.png";
import Translate from "../img/translate.png";
import Germany from "../img/germany.jpeg";
import Berlin from "../img/berlin.jpg";

import '../styles/landing/Homepage.scss'


function Landing({redirect, teacher, authenticated}) {
  /**
   * Landing page. Redirects to dashboard after user has successfully logged in.
   */

  if (redirect) {
    return teacher ? <Redirect exact from="/" to="/teacher/"/> : <Redirect exact from="/" to="/student/"/>
  } else return (
    <div>
      <section className="showcase-container" style={{backgroundImage: `url(${Germany})`, backgroundSize: 'cover'}}>
        {authenticated ? <Link to={teacher ? "/teacher/" : "/student/"}>
            <div className="user-button return-btn">Back to work</div>
          </Link> :
          <AuthAction/>}
      </section>

      <article className="triple-box-container">
        <div>
          <img src={Gate} alt={"Branderbourg gate"}/>
          <p className="column-title">LOREM</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </div>
        <div>
          <img src={Language} alt={"Language symbol"}/>
          <p className="column-title">IPSUM</p>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </div>
        <div>
          <img src={Translate} alt={"Translate symbol"}/>
          <p className="column-title">DOLOR</p>
          <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
        </div>
      </article>

      <article className="about-container">
        <div>
          <p className="about-title">About us</p>
          <br/>
          <p className="about-text">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cupiditate
            vero accusantium distinctio officiis. Libero porro odio vel nesciunt
            amet doloremque nam, sit repellendus. Fugiat architecto, voluptatum
            mollitia autem vero sed?
          </p>
        </div>
        <aside style={{background: `url(${Berlin})`, backgroundPosition: 'center', backgroundSize: 'cover'}}/>
      </article>
    </div>);
}

const mapStateToProps = state => {
  return {
    redirect: state.auth.redirect,
    teacher: state.auth.isTeacher,
    authenticated: state.auth.authToken,
  }
};


export default connect(mapStateToProps, null)(Landing);