import {store} from "../../index";
import {setErrorMessage, setSuccessMessage} from "./popupSlice";


export function notifyError(message, timeout = 3000) {
  store.dispatch(setErrorMessage({message: message}));
  setTimeout(() => {
    store.dispatch(setErrorMessage({message: ""}));
  }, timeout)
}

export function notifySuccess(message, timeout = 3000) {
  store.dispatch(setSuccessMessage({message: message}));
  setTimeout(() => {
    store.dispatch(setSuccessMessage({message: ""}));
  }, timeout)
}