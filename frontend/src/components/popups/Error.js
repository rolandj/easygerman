import React from "react";
import {connect} from "react-redux";

import "../../styles/popups/Error.scss"


function Error({message}) {
  return message ? (
    <div className="error">
      <p>{message}</p>
    </div>
  ) : null;
}

const mapStateToProps = state => {
  return {
    message: state.popups.errorMessage
  }
};


export default connect(mapStateToProps, null)(Error);