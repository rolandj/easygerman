import React from "react";

import "../../styles/popups/Success.scss"
import {connect} from "react-redux";


function Success({message}) {
  return message ? (
    <div className="success">
      <p>{message}</p>
    </div>
  ) : null;
}

const mapStateToProps = state => {
  return {
    message: state.popups.successMessage
  }
};


export default connect(mapStateToProps, null)(Success);