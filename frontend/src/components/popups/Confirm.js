import React from "react";
import {connect} from "react-redux";

import "../../styles/popups/Confirm.scss"
import {setActionStatuses} from "./popupSlice";


function Confirm({message, setStatus}) {
  return message ? (
    <div className="confirm">
      <p className="message">{message}</p>
      <div className="choice-container">
        <p className="confirm-true" onClick={() => setStatus({confirmed: true, canceled: false})}>Yes</p>
        <p className="confirm-reject" onClick={() => setStatus({confirmed: false, canceled: true})}>Cancel</p>
      </div>
    </div>
  ) : null;
}

const mapStateToProps = state => {
  return {
    message: state.popups.confirmMessage,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setStatus: data => dispatch(setActionStatuses(data)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Confirm);