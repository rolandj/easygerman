import {createSlice} from "@reduxjs/toolkit";


const popupSlice = createSlice({
  name: 'popups',
  initialState: {
    errorMessage: "",
    successMessage: "",
    confirmMessage: "",
    actionConfirmed: false,
    actionCanceled: false,
  },
  reducers: {
    setErrorMessage: (state, action) => {
      state.errorMessage = action.payload.message;
    },
    setSuccessMessage: (state, action) => {
      state.successMessage = action.payload.message;
    },
    setConfirmMessage: (state, action) => {
      state.confirmMessage = action.payload.message;
    },
    setActionStatuses: (state, action) => {
      state.actionConfirmed = action.payload.confirmed;
      state.actionCanceled = action.payload.canceled;
    },
  },
});


export const {setErrorMessage, setSuccessMessage, setConfirmMessage, setActionStatuses} = popupSlice.actions;
export default popupSlice.reducer;
