import React, {useState} from "react";
import {connect} from "react-redux";

import {apiRegister} from "./authSlice";
import {notifyError} from "../popups/Notifications";


function Register({register}) {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");


  const sendForm = e => {
    e.preventDefault();
    if (password !== passwordConfirm) {
      notifyError("Passwords don't match");
    } else if (password.length < 8) {
      notifyError("Password must contain minimum 8 characters");
    } else {
      register({username: username, email: email, password: password});
    }
  };


  return <form onSubmit={sendForm}>
    <label>Username:
      <input type="text"
             name="username"
             value={username}
             onChange={e => setUsername(e.target.value)}/>
    </label>
    <label>Email:
      <input type="text"
             name="email"
             value={email}
             onChange={e => setEmail(e.target.value)}/>
    </label>
    <label>Password:
      <input type="password"
             name="password"
             value={password}
             onChange={e => setPassword(e.target.value)}/>
    </label>
    <label>Confirm password:
      <input type="password"
             name="passwordConfirm"
             value={passwordConfirm}
             onChange={e => setPasswordConfirm(e.target.value)}/>
    </label>
    <button>Submit</button>
  </form>
}


const mapDispatchToProps = dispatch => {
  return {
    register: data => dispatch(apiRegister(data))
  }
};

export default connect(null, mapDispatchToProps)(Register);