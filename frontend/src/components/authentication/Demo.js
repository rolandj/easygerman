import React, {useState} from "react";
import {connect} from "react-redux";

import {apiLogin} from "./authSlice";
import "../../styles/landing/Demo.scss"


function Demo({login}) {
  /**
   * Logs user in, with special demo account for demonstration purposes.
   */
  const [buttonClicked, setClicked] = useState(false);

  const loginDemo = (teacher) => {
    if (teacher) {
      login({username: 'teacher', password: 'teacher'});
    } else {
      login({username: 'student', password: 'student'});
    }
  };


  if (buttonClicked) {
    return <div className="demo-select">
      <button className="demo-teacher" onClick={() => loginDemo(true)}>Teacher</button>
      <button className="demo-student" onClick={() => loginDemo(false)}>Student</button>
    </div>
  }
  return (
    <div>
      <button onClick={() => setClicked(true)}>
        Demo
      </button>
    </div>
  );
}


const mapDispatchToProps = dispatch => {
  return {
    login: data => dispatch(apiLogin(data)),
  }
};

export default connect(null, mapDispatchToProps)(Demo);