import {createSlice} from "@reduxjs/toolkit";
import {createAsyncThunk} from "@reduxjs/toolkit";

import fetchClient from "../FetchClient";
import {notifyError, notifySuccess} from "../popups/Notifications";


export const apiLogin = createAsyncThunk(
  'auth/apiLoginStatus',
  async data => {
    try {
      const response = await fetchClient.post('users/login/', data);
      if (response.data) {
        localStorage.setItem('username', response.data.username);
        localStorage.setItem('authToken', response.data.token);
        localStorage.setItem('isTeacher', response.data.is_teacher);
      }
      return response.data
    } catch (e) {
      if (e.response.status === 400) {
        notifyError("Invalid username or password")
      }
    }
  }
);

export const apiRegister = createAsyncThunk(
  'auth/apiRegisterStatus',
  async data => {
    try {
      const response = await fetchClient.post('users/', data);
      if (response.data) {
        localStorage.setItem('username', response.data.username);
        localStorage.setItem('authToken', response.data.token);
        localStorage.setItem('isTeacher', response.data.is_teacher);
      }
      notifySuccess("Email sent, please activate account.")
      return response.data
    } catch (e) {
      if (e.response.status === 500) {
        notifyError("Service unavailable")
      }
    }
  }
);

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    username: localStorage.getItem('username'),
    isTeacher: JSON.parse(localStorage.getItem('isTeacher')),
    authToken: localStorage.getItem('authToken'),
    redirect: false,
  },
  reducers: {
    toggleRedirect: state => state.redirect = !state.redirect,
    clearData: state => {
      state.username = "";
      state.isTeacher = "";
      state.authToken = "";
      state.redirect = false;
    }
  },
  extraReducers: {
    [apiLogin.fulfilled]:
      (state, action) => {
        if (action.payload) {
          state.redirect = true;
          state.username = action.payload.username;
          state.isTeacher = action.payload.is_teacher;
          state.authToken = action.payload.token;
        }
      },
    [apiRegister.fulfilled]:
      (state, action) => {
        if (action.payload.activated) {
          state.redirect = true;
          state.username = action.payload.username;
          state.isTeacher = action.payload.is_teacher;
          state.authToken = action.payload.token;
        }
      }
  }
});


export const {clearData} = authSlice.actions;
export default authSlice.reducer;