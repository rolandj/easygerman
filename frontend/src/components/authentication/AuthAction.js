import React, {useState} from "react";

import Login from "./Login";
import Register from "./Register";
import Demo from "./Demo";

import "../../styles/landing/Authentication.scss"


function AuthAction(props) {
  /** Renders `Register` or `Login` component depending on user choice */

  const [signUp, switchToSignUp] = useState(false);

  return (
    <div>
      <h1 className="company-motto">German is easy.<br/>Trust us.</h1>
      <div className="form-area">
        {signUp ? <Register  {...props}/> : <Login {...props}/>}
        <button onClick={() => switchToSignUp(!signUp)}>
          {signUp ? 'Login' : 'Register'}
        </button>
        <Demo {...props}/>
      </div>
    </div>
  );
}

export default AuthAction;