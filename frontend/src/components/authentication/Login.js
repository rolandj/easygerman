import React, {useState} from "react";
import {connect} from "react-redux";

import {apiLogin} from "./authSlice";


function Login({login}) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");


  const sendForm = e => {
    e.preventDefault();
    login({username: username, password: password});
  };

  return <form onSubmit={sendForm}>
    <label>Username or email:
      <input type="text"
             name="username"
             value={username}
             onChange={e => setUsername(e.target.value)}/>
    </label>
    <label>Password:
      <input type="password"
             name="password"
             value={password}
             onChange={e => setPassword(e.target.value)}/>
    </label>
    <button>Submit</button>
  </form>
}

const mapDispatchToProps = dispatch => {
  return {
    login: data => dispatch(apiLogin(data)),
  }
};

export default connect(null, mapDispatchToProps)(Login);