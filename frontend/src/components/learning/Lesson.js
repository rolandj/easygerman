import React, {useEffect, useState} from "react";
import fetchClient from "../FetchClient";

import Board from "./Board";
import Overview from "./Overview";

import {addRepeatedWord, fetchLessons, removeRepeatedWord, startLesson} from "./lessonSlice";
import {connect} from "react-redux";
import {notifyError} from "../popups/Notifications";


function Lesson({username, lessonData, repeatedWords, addRepeated, removeRepeated, selectedCompletedID, selectedLessonID, beginLesson, reloadLessons}) {
  /**
   * Main view to learn words and send grades.
   *
   * Selects words to process on `Board` child component by incrementing local index and iterating over lesson data.
   * When user has iterated through all lesson data, repeats locally words he didn't know.
   */

  const [index, setIndex] = useState(0);
  const [currentWord, setCurrentWord] = useState("");
  const [currentTranslation, setCurrentTranslation] = useState("");
  const [repeating, setRepeating] = useState(false);
  const [finished, setFinished] = useState(false);

  const handleContinueRepeating = () => setIndex(index + 1);

  const handleStopRepeating = () => {
    if (repeatedWords.length === 1) {
      setRepeating(false);
      setFinished(true);
    }
    removeRepeated({index: index});
  };

  useEffect(() => {
    // Fetch lesson data when user picks not completed
    if (selectedLessonID) {
      beginLesson({lessonID: selectedLessonID});
    }
  }, [selectedLessonID]);

  useEffect(() => {
    // Proceed to next word when index is incremented
    if (lessonData) {
      if (repeating) {
        if (index < repeatedWords.length) {
          setCurrentWord(repeatedWords[index].word);
          setCurrentTranslation(repeatedWords[index].translation);
        } else
          setIndex(0);
      } else {
        if (index < lessonData.grades.length) {
          setCurrentWord(lessonData.grades[index].word);
          setCurrentTranslation(lessonData.grades[index].translation);
        } else if (repeatedWords.length) {
          setIndex(0);
          setCurrentWord(repeatedWords[0].word);
          setCurrentTranslation(repeatedWords[0].translation);
          setRepeating(true);
        } else {
          setFinished(true);
        }
      }
    }
  }, [index, lessonData, repeatedWords]);


  if (selectedLessonID && lessonData) {
    return <Board word={currentWord} translation={currentTranslation} sendGrade={sendGrade}
                  finished={finished} repeat={repeating} continueRepeating={handleContinueRepeating}
                  stopRepeating={handleStopRepeating} handleFinish={() => reloadLessons({username: username})}/>
  } else if (selectedCompletedID) {
    return <Overview/>
  } else {
    return null;
  }

  function sendGrade(mode) {
    let data, copy;
    switch (mode) {
      case 'WRONG':
        addRepeated({word: lessonData.grades[index]});
        copy = {...lessonData.grades[index]};
        copy.wrong += 1;
        break;
      case 'MEDIUM':
        addRepeated({word: lessonData.grades[index]});
        copy = {...lessonData.grades[index]};
        copy.medium += 1;
        break;
      case 'GOOD':
        copy = {...lessonData.grades[index]};
        copy.good += 1;
        break;
      default:
        console.log('Select proper grade: "GOOD" | "MEDIUM" | "WRONG"');
    }
    data = {grades: [copy]};
    fetchClient.patch(`/users/${username}/progress/${lessonData.id}/`, data).catch(() => {
      notifyError("Could not save grade.")
    });
    setIndex(index + 1);
  }
}


const mapStateToProps = state => {
  return {
    lessonData: state.lessons.lessonData,
    repeatedWords: state.lessons.repeatedWords,
    currentWord: state.lessons.currentWord,
    currentTranslation: state.lessons.currentTranslation,
    selectedCompletedID: state.lessons.chosenCompletedID,
    selectedLessonID: state.lessons.chosenLessonID,
    username: state.auth.username,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    addRepeated: data => dispatch(addRepeatedWord(data)),
    removeRepeated: data => dispatch(removeRepeatedWord(data)),
    beginLesson: data => dispatch(startLesson(data)),
    reloadLessons: data => dispatch(fetchLessons(data)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Lesson);