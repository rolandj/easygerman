import React, {useEffect} from "react";
import {Link, useRouteMatch} from "react-router-dom";

import "../../styles/learning/List.scss";
import {setCompletedLesson, setLesson, fetchLessons, startLesson} from "./lessonSlice";
import {connect} from "react-redux";


function LessonList({user, lessons, progresses, getLessons, beginLesson, pickCompleted, pickLesson}) {
  /*** List of available lessons.*/

  const {path} = useRouteMatch();

  useEffect(() => {
    if (!lessons.length && !progresses.length) {
      getLessons({username: user});
    }
  });

  return lessons.length || progresses.length ? <div>
      <ul className="progresses-list">
        {renderStarted()}
      </ul>
      <ul className="lesson-list">
        {renderNotStarted()}
      </ul>
    </div>
    : null;


  function renderStarted() {
    return (
      progresses.map((progress, index) =>
        <Link key={index} to={`${path}/learn`}>
          <li key={progress.id} className={progress.percentage === 100 ? "passed-lesson" : ""}
              onClick={() => {
                if (progress.percentage === 100) {
                  localStorage.setItem('studentSelectedCompleted', progress.lesson);
                  localStorage.removeItem('studentSelectedLesson');
                  pickCompleted({lessonID: progress.lesson});
                } else {
                  localStorage.setItem('studentSelectedLesson', progress.lesson);
                  localStorage.removeItem('studentSelectedCompleted');
                  pickLesson({lessonID: progress.lesson});
                }
              }}>
            {progress.lesson_name}
            {progress.percentage === 100 ? <span>(passed)</span> :
              <div className="bar">
                <div className="progress"
                     style={{width: `${progress.percentage}%`}}/>
              </div>}
          </li>
        </Link>
      )
    );
  }

  function renderNotStarted() {
    return (
      lessons.map((lesson, index) =>
        <Link key={index} to={`${path}/learn`}>
          <li key={lesson.id} onClick={() => {
            localStorage.setItem('studentSelectedLesson', lesson.id);
            localStorage.removeItem('studentSelectedCompleted');
            pickLesson({lessonID: lesson.id});
          }}>
            {lesson.name} <span>(new)</span>
          </li>
        </Link>));
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.username,
    lessons: state.lessons.lessons,
    progresses: state.lessons.progresses,
    selectedLesson: state.lessons.lessonID,
    selectedCompletedLesson: state.lessons.completedLessonID,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    pickLesson: data => dispatch(setLesson(data)),
    pickCompleted: data => dispatch(setCompletedLesson(data)),
    getLessons: data => dispatch(fetchLessons(data)),
    beginLesson: data => dispatch(startLesson(data)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(LessonList);
