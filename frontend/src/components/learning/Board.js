import React, {useState} from "react";
import {Link} from "react-router-dom";

import "../../styles/learning/Board.scss"
import "../../styles/learning/Overview.scss"


function Board(props) {
  /**
   * Render current lesson state. That is: processed word, translation, rate buttons or back to menu button.
   */
  const [checked, checkWord] = useState(false);


  if (props.repeat) {
    return <section className="lesson">
      <div className="learning-area">
        <p className="word">{props.word}</p>
        {checked ? <p className="translation">{props.translation}</p> : null}
      </div>
      <div className="action-area">
        {checked ?
          <ul className="grade-box repeating">
            <li className="grade-wrong" onClick={() => {
              checkWord(false);
              props.continueRepeating();
            }}>Don't
            </li>
            <li className="grade-good" onClick={() => {
              checkWord(false);
              props.stopRepeating();
            }}>I know
            </li>
          </ul>
          :
          <div className="user-button check-button" onClick={() => checkWord(true)}>Check</div>}
      </div>
    </section>
  } else if (props.finished) {
    return <div className="finish-text">That's it for today, good job!
      <Link className="user-button lesson-return" onClick={props.handleFinish} to="/student">Back to menu</Link></div>
  } else {
    return <section className="lesson">
      <div className="learning-area">
        <p className="word">{props.word}</p>
        {checked ? <p className="translation">{props.translation}</p> : null}
      </div>
      <div className="action-area">
        {checked ?
          <ul className="grade-box">
            <li className="grade-wrong" onClick={() => {
              checkWord(false);
              props.sendGrade('WRONG');
            }}>Don't
            </li>
            <li className="grade-medium" onClick={() => {
              checkWord(false);
              props.sendGrade('MEDIUM');
            }}>Almost
            </li>
            <li className="grade-good" onClick={() => {
              checkWord(false);
              props.sendGrade('GOOD');
            }}>I know
            </li>
          </ul>
          :
          <div className="user-button check-button" onClick={() => checkWord(true)}>Check</div>}
      </div>
    </section>
  }
}

export default Board;
