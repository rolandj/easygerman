import {createSlice} from "@reduxjs/toolkit";
import {createAsyncThunk} from "@reduxjs/toolkit";

import fetchClient from "../FetchClient";
import {notifyError} from "../popups/Notifications";


export const fetchLessons = createAsyncThunk(
  'auth/fetchingLessonStatus',
  async data => {
    try {
      const [lessons, progresses] = await Promise.all([fetchClient.get('lessons/'),
        fetchClient.get(`users/${data['username']}/progress/`)]);
      // Delete lessons which student has already started (progresses) to avoid duplication
      const lessonsCopy = [...lessons.data];
      for (let i = 0; i < lessonsCopy.length; i++) {
        for (const progress of progresses.data) {
          if (progress['lesson'] === lessons.data[i].id) {
            delete lessons.data[i];
            break;
          }
        }
      }
      return [lessons.data, progresses.data]
    } catch (e) {
      console.log(e);
      notifyError("Service unavailable")
    }
  }
);

export const startLesson = createAsyncThunk(
  'auth/startLessonStatus',
  async data => {
    try {
      const response = await fetchClient.get(`lessons/${data.lessonID}/start/`);
      return response.data
    } catch (e) {
      console.log(e);
      notifyError("Service unavailable")
    }
  }
);

const lessonSlice = createSlice({
    name: 'lessons',
    initialState: {
      lessons: [],
      progresses: [],
      chosenLessonID: JSON.parse(localStorage.getItem('studentSelectedLesson')),
      chosenCompletedID: JSON.parse(localStorage.getItem('studentSelectedCompleted')),
      repeatedWords: [],
      lessonData: null,
    },
    reducers: {
      setLesson: (state, action) => {
        state.chosenLessonID = action.payload.lessonID;
        state.chosenCompletedID = null;
      },
      setCompletedLesson: (state, action) => {
        state.chosenCompletedID = action.payload.lessonID;
        state.chosenLessonID = null;
      },
      clearChosenLesson: (state, action) => {
        state.completedLessonID = null;
        state.chosenLessonID = null;
        state.lessonData = null;
      },
      addRepeatedWord: (state, action) => {
        state.repeatedWords.push(action.payload.word)
      },
      removeRepeatedWord: (state, action) => {
        state.repeatedWords = state.repeatedWords.filter((w, index) => index !== action.payload.index)
      },
    },
    extraReducers: {
      [fetchLessons.fulfilled]:
        (state, action) => {
          state.lessons = action.payload[0];
          state.progresses = action.payload[1];
        },
      [startLesson.fulfilled]:
        (state, action) => {
          state.lessonData = action.payload;
          state.repeatedWords = []; // Implicitly clear repeated words to prevent data leakage
        }
    }
  })
;


export const {setLesson, setCompletedLesson, clearChosenLesson, addRepeatedWord, removeRepeatedWord} = lessonSlice.actions;
export default lessonSlice.reducer;