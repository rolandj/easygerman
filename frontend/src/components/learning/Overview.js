import React, {useEffect, useState} from "react";
import fetchClient from "../FetchClient";

import {connect} from "react-redux";
import {Redirect} from "react-router-dom";
import {setActionStatuses, setConfirmMessage} from "../popups/popupSlice";
import {notifyError} from "../popups/Notifications";
import {fetchLessons, clearChosenLesson} from "./lessonSlice";


function Overview({lessonID, canceled, confirmed, username, showConfirmMessage, setStatuses, reloadLessons, clearChoices}) {
  /**
   * A view rendered when user has passed lesson.
   *
   * Allows user to see lesson data or reset recorded progress.
   */

  const [redirect, setRedirect] = useState(false);
  const [lessonData, setLessonData] = useState();
  const [resetting, setResettingStatus] = useState(false);


  useEffect(() => {
    // Fetch lesson data to render
    if (lessonID) {
      fetchClient.get(`lessons/${lessonID}/`)
        .then(response => setLessonData(response.data))
        .catch(() => notifyError("Could not load lesson")
        );
    }
  }, [lessonID]);

  useEffect(() => {
    // Reset lesson when user click reset button
    if (resetting) {
      showConfirmMessage({message: "Are you sure to reset lesson?"});
      if (confirmed) {
        fetchClient.post(`lessons/${lessonID}/reset/`)
          .then(() => {
            setResettingStatus(false);
            showConfirmMessage({message: ""});
            setRedirect(true);
            reloadLessons({username: username});
            clearChoices();
            setStatuses({confirmed: false, canceled: false});
            localStorage.removeItem('studentSelectedCompleted');
            localStorage.removeItem('studentSelectedLesson');
          })
          .catch(() => notifyError("Could not reset lesson")
          );
      } else if (canceled) {
        setStatuses({canceled: false, confirmed: false});
        setResettingStatus(false);
        showConfirmMessage({message: ''});
      }
    }
  }, [confirmed, canceled, resetting]);


  if (redirect) {
    return <Redirect from="/student/learn" to="/student"/>
  } else {
    return lessonData ?
      <section className="lesson-overview">
        <h2>{lessonData.name} overview:</h2>
        <table>
          <tbody>
          <tr>
            <th>Word</th>
            <th>Translation</th>
          </tr>
          {lessonData.words.map((word, index) =>
            <tr key={index}>
              <td>{word.word} </td>
              <td>{word.translation}</td>
            </tr>)}
          </tbody>
        </table>
        <div className="user-button reset-lesson" onClick={() => setResettingStatus(true)}>Reset lesson</div>
      </section>
      : null;
  }
}

const mapStateToProps = state => {
  return {
    canceled: state.popups.actionCanceled,
    confirmed: state.popups.actionConfirmed,
    lessonID: state.lessons.chosenCompletedID,
    username: state.auth.username,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    showConfirmMessage: data => dispatch(setConfirmMessage(data)),
    setStatuses: data => dispatch(setActionStatuses(data)),
    reloadLessons: data => dispatch(fetchLessons(data)),
    clearChoices: () => dispatch(clearChosenLesson()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Overview);