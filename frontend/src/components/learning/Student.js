import React from "react";
import {Route, Switch, useRouteMatch} from "react-router-dom";

import LessonList from "./LessonList";
import Lesson from "./Lesson";


function Student() {
  /**
   * Render list of available lessons or specified lesson.
   */

  const {path} = useRouteMatch();


  return (<div>
    <Switch>
      <Route exact path={path}>
        <LessonList/>
      </Route>
      <Route path={`${path}/learn`}>
        <Lesson/>
      </Route>
      <Route path="/"><h1>Page not found.</h1></Route>
    </Switch>
  </div>);
}


export default Student;
