import axios from "axios";


const production = 'https://easygerman.herokuapp.com/api';
const development = 'http://127.0.0.1:8000/api';

const fetchClient = axios.create({
    baseURL: process.env.NODE_ENV === "development" ? development : production
  }
);

fetchClient.interceptors.request.use((config) => {
  const token = localStorage.getItem('authToken');
  config.headers.Authorization = token ? `Token ${token}` : '';
  return config;
});

export default fetchClient;
