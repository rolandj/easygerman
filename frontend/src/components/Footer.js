import React from "react";

import "../styles/landing/Footer.scss";


function Footer(props) {
  const displayInfo = () => props.printErrorMessage("Section not implemented yet.");

  return <footer>
    <div className="footer-container">
      <div className="footer-text">
        <h2>We're best language learning platform.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
        </p>
      </div>
      <ul>
        <li onClick={displayInfo}>Contact</li>
        <li onClick={displayInfo}>Twitter</li>
        <li onClick={displayInfo}>Instagram</li>
      </ul>
    </div>
    <div className="footer-copyright">Copyright &copy; 2020 Songoor</div>
  </footer>
}

export default Footer;