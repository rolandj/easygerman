import React, {useEffect, useState} from "react";
import {Link, Redirect} from "react-router-dom";
import {connect} from "react-redux";

import logo from "../img/logo.png"
import "../styles/landing/Nav.scss";
import {clearData} from "./authentication/authSlice";
import {notifySuccess, notifyError} from "./popups/Notifications";
import {setConfirmMessage, setActionStatuses} from "./popups/popupSlice";


function Nav({loggedIn, confirmed, canceled, showConfirmMessage, setStatuses, clearUserData}) {
  const [redirect, setRedirect] = useState(false);
  const [loggingOut, setLoggingOut] = useState(false);


  useEffect(() => {
    // Handle logging out
    if (loggedIn && loggingOut && confirmed) {
      setStatuses({confirmed: false, canceled: false});
      localStorage.clear();
      clearUserData();
      showConfirmMessage({message: ""});
      notifySuccess("Successfully logged out");
      setRedirect(true);
      setLoggingOut(false);
    } else if (loggedIn && loggingOut && canceled) {
      showConfirmMessage({message: ""});
      setStatuses({confirmed: false, canceled: false});
      setLoggingOut(false);
    } else if (!canceled && !confirmed) {
      setRedirect(false);
    }
  }, [loggedIn, confirmed, canceled]);

  const logout = () => {
    setLoggingOut(true);
    showConfirmMessage({message: "Are you sure to logout?"})
  };

  return redirect ? <Redirect to={"/"}/> : <header>
    <nav>
      <ul>
        <li>
          <Link to="/"><img src={logo} alt="Home button"/></Link>
        </li>
        {loggedIn ?
          <li onClick={logout}><span className="button-logout">Logout</span></li> : ""}
        <li onClick={() => notifyError("Section not implemented yet.")}>About</li>
        <li onClick={() => notifyError("Section not implemented yet.")}>FAQ</li>
      </ul>
    </nav>
  </header>
}

const mapDispatchToProps = dispatch => {
  return {
    showConfirmMessage: data => dispatch(setConfirmMessage(data)),
    setStatuses: data => dispatch(setActionStatuses(data)),
    clearUserData: () => dispatch(clearData()),
  }
};

const mapStateToProps = state => {
  return {
    loggedIn: state.auth.authToken,
    confirmed: state.popups.actionConfirmed,
    canceled: state.popups.actionCanceled,
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Nav);