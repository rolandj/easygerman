import React from "react";

import {Route, Switch} from "react-router-dom";

import Nav from "./Nav";
import Footer from "./Footer";
import Landing from "./Landing";
import Student from "./learning/Student"
import TeacherRouter from "./teaching/TeacherRouter";
import Error from "./popups/Error";
import Success from "./popups/Success";
import Confirm from "./popups/Confirm";


function App() {
  return (<div>
    <main>
      <Nav />
      <Switch>
        <Route exact path="/(|activate/success/)">
          <Landing />
        </Route>
        <Route path="/teacher">
          <TeacherRouter/>
        </Route>
        <Route path="/student">
          <Student />
        </Route>
        <Route path="/"><h1>Page not found.</h1></Route>
      </Switch>
      <Confirm />
      <Error />
      <Success />
    </main>
    <Footer />
  </div>);
}

export default App;