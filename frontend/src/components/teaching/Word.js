import React, {useEffect, useState} from "react";


function Word(props) {
  /**
   * Represents word input of Word instance.
   */

  const [word, setWord] = useState(props.word);

  useEffect(() => {
    if (word) {
      props.handleChange(word);
    }
  }, [word, props]);

  return (
    <div>
      <label>Word:
        <input type="text"
               name="word"
               spellCheck="false"
               value={word}
               onChange={e => setWord(e.target.value)}/>
      </label>
    </div>);
}

export default Word;