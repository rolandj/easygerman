import React, {useEffect, useState} from "react";
import {Route, Switch, useRouteMatch} from "react-router-dom";

import EditPanel from "./EditPanel";
import TeacherLessonList from "./TeacherLessonList";


function TeacherRouter(props) {
  /**
   * Render list of available lessons or panel to edit specified lesson.
   * Mediates `selectedLesson` and `initialise` state variables between list and panel.
   *
   * @return TeacherLessonList when no lesson selected.
   * @return EditPanel when lesson chose by user.
   */

  const {path} = useRouteMatch();
  const [creatingNew, setCreatingNew] = useState(false);
  const [initialise, setInitialising] = useState(true);
  const [selectedLessonID, selectLesson] = useState();

  useEffect(() => {
    const selectedLesson = JSON.parse(localStorage.getItem("teacherSelectedLesson"));
    selectLesson(selectedLesson);
  }, [props]);

  useEffect(() => {
    if (initialise) {
      setInitialising(false);
    }
  }, [initialise]);

  const handleLessonSelect = (id) => {
    selectLesson(id);
    localStorage.setItem("teacherSelectedLesson", id);
  };

  const handleCreated = () => {
    setInitialising(true);
    setCreatingNew(false);
  };

  const handleCreating = () => setCreatingNew(true);


  return <div>
    <Switch>
      <Route exact path={`${path}`}>
        <TeacherLessonList handleLessonSelect={handleLessonSelect}
                           reload={initialise}
                           createNew={handleCreating}/>
      </Route>
      <Route exact path={`${path}/lesson`}>
        <EditPanel lessonID={selectedLessonID}
                   createNew={creatingNew}
                   setCreated={handleCreated}/>
      </Route>
      <Route path="/"><h1>Page not found.</h1></Route>
    </Switch>
  </div>
}

export default TeacherRouter;