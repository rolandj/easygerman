import React, {useEffect, useState} from "react";


function Translation(props) {
  /**
   * Represents translation input of Word instance.
   */

  const [translation, setTranslation] = useState(props.translation);

  useEffect(() => {
    if (translation) {
      props.handleChange(translation);
    }
  }, [translation, props]);

  return (
    <div>
      <label>Translation:
        <input type="text"
               name="translation"
               spellCheck="false"
               value={translation}
               onChange={e => setTranslation(e.target.value)}/>
      </label>
    </div>
  );
}

export default Translation;