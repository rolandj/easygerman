import React, {useEffect, useState} from "react";
import {Link, useRouteMatch} from "react-router-dom";
import {connect} from "react-redux";

import fetchClient from "../FetchClient";
import {setConfirmMessage} from "../popups/popupSlice";
import {setActionStatuses} from "../popups/popupSlice";
import {notifyError, notifySuccess} from "../popups/Notifications";
import Cross from "../../img/cross.png";
import "../../styles/teaching/TeacherList.scss"


function TeacherLessonList({confirmed, canceled, reload, setConfirm, setStatuses, createNew, handleSelect}) {
  /**
   * List of available lessons.
   *
   * Allows choose instance to edit / delete or create new lesson.
   */
  const {path} = useRouteMatch();
  const [initialising, setInitialising] = useState(true);
  const [lessons, setLessons] = useState(false);
  const [deletedLessonID, selectDeleting] = useState("");


  useEffect(() => {
    // Fetch and prepare data for rendering
    if (initialising || reload) {
      // Read previous session data from localstorage
      fetchClient.get(`lessons/`)
        .then(response => setLessons(response.data))
        .catch(() => notifyError("Could not load data from server."));
      setInitialising(false);
    }
  }, [initialising, reload]);


  useEffect(() => {
    // Handle deleting lesson instance
    if (deletedLessonID) {
      if (confirmed) {
        fetchClient.delete(`lessons/${deletedLessonID}/`).then(() => {
          notifySuccess("Lesson deleted successfully.");
          selectDeleting('');
          setConfirm({message: ''});
          setInitialising(true);
          setStatuses({confirmed: false, canceled: false});
          }
        ).catch(() => notifyError("Could not delete lesson."));
      } else if (canceled) {
        selectDeleting("");
        setConfirm({message: ''});
        setStatuses({confirmed: false, canceled: false});
      } else {
        setConfirm({message: "Are you sure to delete this lesson?"});
      }
    }
  }, [deletedLessonID, confirmed, canceled]);


  return lessons ?
    <div>
      <ul className="teacher-lesson-list">{
        lessons.map((lesson, index) =>
          <div className="teacher-lesson-element" key={index}>
            <Link to={`${path}/lesson`}>
              <li onClick={() => handleSelect(lesson.id)}>
                {lesson.name}
              </li>
            </Link>
            <img onClick={() => selectDeleting(lesson.id)} src={Cross} alt="Delete button"/>
          </div>
        )}
      </ul>

      <Link onClick={createNew} to={`${path}/lesson`}>
        <div className="user-button add-button">Add new</div>
      </Link>

    </div>
    : null;
}

const mapStateToProps = (state, ownProps) => {
  return {
    confirmed: state.popups.actionConfirmed,
    canceled: state.popups.actionCanceled,
    reload: ownProps.reload,
  }
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setConfirm: data => dispatch(setConfirmMessage(data)),
    setStatuses: data => dispatch(setActionStatuses(data)),
    createNew: ownProps.createNew,
    handleSelect: ownProps.handleLessonSelect,
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(TeacherLessonList);