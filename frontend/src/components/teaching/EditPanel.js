import React, {useEffect, useReducer, useState} from "react";
import {Redirect} from "react-router-dom";

import fetchClient from "../FetchClient";
import Translation from "./Translation";
import Word from "./Word";

import Cross from "../../img/cross.png";
import "../../styles/teaching/EditPanel.scss"
import {notifyError, notifySuccess} from "../popups/Notifications";


function EditPanel(props) {
  /**
   * Allows teacher to create / update lesson.
   *
   * Each lesson contains Word instances. Each Word object as fundamental datatype informs about:
   *  - actual word (rendered and processed by Word component)
   *  - its translation (rendered and processed by Translation component)
   */

  const [initialising, setInitialising] = useState(true);
  const [redirecting, setRedirecting] = useState(false);
  const [lessonName, setLessonName] = useState("");
  const [lessonData, dispatchLesson] = useReducer((lessonData, {type, value}) => {
    switch (type) {
      case "init":
        value.forEach((w, i) => w.index = i);
        return [...lessonData, ...value];
      case "add":
        if (lessonData.length) {
          value.index = lessonData[lessonData.length - 1].index + 1;
        } else {
          value.index = 0
        }
        return [...lessonData, value];
      case "updateWord":
        lessonData[value.index].word = value.word;
        return lessonData;
      case "updateTranslation":
        lessonData[value.index].translation = value.translation;
        return lessonData;
      case "remove":
        return lessonData.filter((w, index) => index !== value);
      default:
        return lessonData;
    }
  }, []);

  useEffect(() => {
    // Fetch server to get lesson fo further processing
    if (initialising && props.lessonID && !props.createNew) {
      fetchClient.get(`lessons/${props.lessonID}/`)
        .then(response => {
          setLessonName(response.data.name);
          dispatchLesson({type: "init", value: response.data.words});
          setInitialising(false);
        })
        .catch(() => notifyError("Server unavailable."));
    }
  }, [props, initialising]);


  const handleAddingWord = () => {
    if (!hasEmptyInput()) {
      dispatchLesson({type: "add", value: {word: "", translation: ""}});
    } else {
      notifyError("Please fill empty fields.")
    }
  };

  const handleDeletingWord = (index) => {
    if (lessonData.length > 1) {
      dispatchLesson({type: "remove", value: index})
    } else {
      notifyError("Lesson must not be empty.")
    }
  };

  const handleUpdatingWord = (w, key) => dispatchLesson({
    type: 'updateWord',
    value: {word: w, index: key}
  });

  const handleUpdatingTranslation = (t, key) => dispatchLesson({
    type: 'updateTranslation',
    value: {translation: t, index: key}
  });

  if (redirecting) {
    return <Redirect from="/teacher/lesson" to="/teacher"/>;
  } else {
    return lessonData.length || props.createNew ? (
      <section className="lesson-panel">
        <form onSubmit={props.createNew ? sendLesson : patchLesson}>
          <label>Lesson name:
            {props.createNew ?
              <input value={lessonName} onChange={e => setLessonName(e.target.value)}/> : <p>{lessonName}</p>}
          </label>
          <div className="word-list">
            {lessonData.map((word, index) =>
              <div key={word.index} className="word-form">
                <Word word={word.word} handleChange={w => handleUpdatingWord(w, index)}/>
                <Translation translation={word.translation}
                             handleChange={t => handleUpdatingTranslation(t, index)}/>
                <img onClick={() => handleDeletingWord(index)} src={Cross} alt="Delete button"/>
              </div>)}
          </div>
          <div className="user-button lesson-add-word" onClick={handleAddingWord}>Add new word</div>
          <button className="user-button send-button">Submit</button>
        </form>
      </section>) : null;
  }

  function hasEmptyInput() {
    if (!lessonName) {
      return true;
    }
    for (const word of lessonData) {
      if (word.word === "" || word.translation === "") {
        return true;
      }
    }
    return false;
  }

  function patchLesson(e) {
    e.preventDefault();
    if (!hasEmptyInput()) {
      fetchClient.patch(`lessons/${props.lessonID}/`, {'words': lessonData})
        .then(() => notifySuccess("Lesson successfully updated."))
        .catch(() => notifyError("Could not update lesson."))
    } else {
      notifyError("Please fill in empty fields.");
    }
  }

  function sendLesson(e) {
    e.preventDefault();
    if (!hasEmptyInput()) {
      setRedirecting(true);
      const data = lessonData;
      data.forEach(w => delete w.index);
      fetchClient.post(`lessons/`, {'name': lessonName, 'words': data})
        .then(response => {
          notifySuccess("Lesson created successfully");
          props.setCreated();
        }).catch(() => notifyError("Could not create lesson."))
    } else {
      notifyError("Please fill in empty fields.");
    }
  }
}


export default EditPanel;